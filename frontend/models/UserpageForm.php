<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\helpers\VarDumper;

/**
 * Signup form
 */
class UserpageForm extends Model
{
    public $username;
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique',
            'targetAttribute' => 'username',
            'targetClass' => '\common\models\User',
            'message' => 'This username can not be taken.',
            'when' => function ($model) {
                return $model->username != Yii::$app->user->identity->username;
            }],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique',
                'targetAttribute' => 'email',
                'targetClass' => '\common\models\User',
                'message' => 'This email can not be taken.',
                'when' => function ($model) {
                    return $model->email != Yii::$app->user->identity->email;
                }],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function update()
    {
        $user = User::find()
            ->where(['id' => Yii::$app->user->identity->id])
            ->one();
        if ($user->username == $this->username && $this->email == $user->email) {
            return null;
        }

        if (!$this->validate()) {
            return null;
        }

        $user->username = $this->username;
        $user->email = $this->email;
        return $user->save();
    }

}
