<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'User page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Welcome to your user page, <?= Yii::$app->user->identity->username ?> </h2>

    <p>Here you can change your login or email data</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-userpage']); ?>

            <?php
            $model->username=Yii::$app->user->identity->username;
            ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?php
            $model->email=Yii::$app->user->identity->email;
            ?>

            <?= $form->field($model, 'email') ?>

            <div class="form-group">
                <?= Html::submitButton('Confirm', ['class' => 'btn btn-success', 'name' => 'userpage-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
