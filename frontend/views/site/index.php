<?php

/* @var $this yii\web\View */

use yii\helpers\Html;


$this->title = 'My Yii Application';
?>


<!-- Main jumbotron for a primary marketing message or call to action -->
<div>
    <div class="jumbotron main ">
        <div class="container ">
            <div class="row">
                <div class="col align-self-center">
                    <h1 class="display-3 ">Welcome!</h1>
                    <p>Try chatting with other programmers online</p>
                    <p>
                        <a class="btn btn-hover color-3"  href="<?php echo (Yii::$app->user->isGuest)? '/user/login': '/site/chats'; ?>" >Start chatting</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="">
        <!-- Example row of columns -->
        <div class="skew-c"></div>
        <div class="colour-block">
            <div>
                <h2 class="colour-header">Last chats:</h2>
                <ul class="list-group">
                    <?php foreach ($chats as $chat): ?>
                        <li class="list-group-item">
                            <a href="#" class="list-link">
                                <?php echo($chat['title']) ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <a class="btn btn-hover color-1"  href="<?php echo (Yii::$app->user->isGuest)? '/user/login': '/site/chats'; ?>" >Show more</a>
        </div>
        <div class="skew-cc"></div>
        <div class="white-block"></div>
    </div> <!-- /container -->
</div>