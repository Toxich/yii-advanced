<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Chats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-list container">
    <h1>You can enter any existing chat or create a new one</h1>
    <div class="colour-block">
        <div>
            <h2 class="colour-header">All chats:</h2>
            <ul class="list-group">
                <?php foreach ($chats as $chat): ?>
                    <li class="list-group-item">
                        <a href="#" class="list-link">
                            <?php echo($chat['title']) ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="white-block"></div>
</div>
