<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chats_messages}}`.
 */
class m200421_102657_create_chats_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chats_messages}}', [
            'id' => $this->primaryKey(),
            'message' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'chat_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%chats_messages}}');
    }
}
